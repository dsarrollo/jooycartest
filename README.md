# JooycarTest

Prueba de conocimiento para el puesto de backend en nodejs

## Name
JooycarTest

## Description
Backend desarrollado en nodejs que muestra lista de viajes(trips), crea viajes a partir de escrituras(readings), como es una prueba de conocimiento solo cuenta con 3 endpoints.

## Installation
1. Instalar nodejs en su version 12.22.9 o superior.
2. Instalar Mongodb Base de datos
3. npm i

## Usage
clonar desde la rama test, luego debe instalar los paquetes necesarios usando este comando "npm i" en consola
podra inciar con los comandos start (para produccion), dev (para desarrollo), test (para pruebas)
1. npm run start 
2. npm run dev 
3. npm run test 

## Project status
Completado
